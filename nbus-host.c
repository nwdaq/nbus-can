/* SPDX-License-Identifier: BSD-2-Clause
 *
 * nbus host-side zeromq bridge
 * OpenWRT nwDaq companion project
 *
 * Copyright (c) 2023, Marek Koza (qyx@krtko.org)
 * All rights reserved.
 */


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <pthread.h>
#include <sys/time.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "nbus-host.h"
#include "nbus-channel.h"
#include "nbus-packet.h"



/**********************************************************************************************************************
 * Descriptor processing
 **********************************************************************************************************************/

static void *nbus_host_descriptor_thread(void *p) {
	NbusHost *self = p;

	while (true) {
		NbusChannel *c = self->channels;
		while (c != NULL) {
			nbus_channel_get_descriptors(c);
			c = c->next;
		}

		sleep(30);
	}
}


/**********************************************************************************************************************
 * nbus layer 3 packet implementation (receive and transmit)
 **********************************************************************************************************************/


static NbusChannel *add_channel(NbusHost *self) {
	NbusChannel *channel = calloc(1, sizeof(NbusChannel));
	if (channel == NULL) {
		return NULL;
	}
	nbus_channel_init(channel, self);
	/* Note that addition is atomic (from the pov of iterating the list) */
	channel->next = self->channels;
	self->channels = channel;
	return channel;
}


nbus_ret_t nbus_host_invalidate_channel(NbusHost *self, NbusChannel *c) {
	c->channel_id_valid = false;
	syslog(LOG_INFO, "channel %08x(%02x) inactive/invalid", c->short_id, c->channel_id);
	return NBUS_RET_OK;
}


nbus_ret_t nbus_host_remove_old_channels(NbusHost *self) {
	struct timespec ts = {0};
	clock_gettime(CLOCK_REALTIME, &ts);

	NbusChannel *c = self->channels;
	while (c != NULL) {
		if ((ts.tv_sec - c->adv_time) > NBUS_ADV_TIMEOUT_S && c->channel_id_valid) {
			nbus_host_invalidate_channel(self, c);
		}
		c = c->next;
	}
}


NbusChannel *nbus_host_find_channel(NbusHost *self, nbus_channel_short_id_t short_id) {
	nbus_host_remove_old_channels(self);

	NbusChannel *c = self->channels;
	while (c != NULL) {
		if (c->short_id == short_id) {
			return c;
		}
		c = c->next;
	}
	return NULL;
}


NbusChannel *nbus_host_find_channel_act(NbusHost *self, nbus_channel_id_t channel_id) {
	NbusChannel *c = self->channels;
	while (c != NULL) {
		if (c->channel_id == channel_id && c->channel_id_valid) {
			return c;
		}
		c = c->next;
	}
	return NULL;
}




static nbus_ret_t nbus_host_can_rx_process(NbusHost *self, struct can_frame *frame) {
	if (!(frame->can_id & (1 << 31))) {
		/* Not an extended ID frame. */
		return NBUS_RET_INVALID;
	}
	frame->can_id &= ~(1 << 31);

	struct nbus_id id;
	nbus_parse_id(frame->can_id, &id);

	// printf("<--- frame channel=0x%04x, opcode=0x%02x, direction=%d, len=%d\n", id.channel, id.opcode, id.direction, frame->can_dlc);

	if (id.opcode == NBUS_OP_ADVERTISEMENT) {
		nbus_channel_short_id_t *short_id = (nbus_channel_short_id_t *)&frame->data;
		NbusChannel *channel = nbus_host_find_channel(self, *short_id);
		if (channel == NULL) {
			channel = add_channel(self);
			syslog(LOG_INFO, "adding channel %08x(%02x) to the list of active channels", *short_id, id.channel);
		} else if (channel->channel_id_valid == false) {
			/* Channel found but invalid. Make valid again. */
			channel->channel_id_valid = true;
			syslog(LOG_INFO, "channel %08x(%02x) active again", *short_id, id.channel);
		}
		if (channel == NULL) {
			return NBUS_RET_FAILED;
		}
		/* Update the mapping channel_id -> short_id */
		channel->channel_id = id.channel;
		channel->channel_id_valid = true;
		channel->short_id = *short_id;

		struct timespec ts = {0};
		clock_gettime(CLOCK_REALTIME, &ts);
		channel->adv_time = ts.tv_sec;
	} else {
		/* Try to find the corresponding channel. Do nothing if not found. */
		NbusChannel *channel = nbus_host_find_channel_act(self, id.channel);
		if (channel == NULL) {
			return NBUS_RET_VOID;
		}

		/** @todo move to the channel class */
		/* Push the frame into the RX packet FSM. Check the result. */
		nbus_ret_t ret = nbus_rxpacket_append(&channel->rxpacket, &id, frame->data, frame->can_dlc);
		if (ret == NBUS_RET_OK) {
			if (channel->rxpacket.state == NBUS_RXP_DATA) {
				/* Reception started, block any transmit attempt. */
				/** @todo lock here */
			} else if (channel->rxpacket.state == NBUS_RXP_DONE) {
				/* The packet is complete, unlock the receiving tread if required. */
				/** @todo unlock rx */
			}
		} else if (ret == NBUS_RET_INVALID) {
			channel->channel_id_valid = false;
		}



	}

	return NBUS_RET_OK;
}


static void *nbus_host_can_rx_thread(void *p) {
	NbusHost *self = p;

	while (true) {
		struct can_frame frame = {0};
		int nbytes = read(self->socket, &frame, sizeof(struct can_frame));
		if (nbytes < 0) {
			/* Error, nothing read or timeout */
			continue;
		} else if (nbytes == 0) {
			continue;
		} else {
			nbus_host_can_rx_process(self, &frame);
		}
	}
}



nbus_ret_t nbus_host_init(NbusHost *self, const char *device, uint32_t bitrate) {
	memset(self, 0, sizeof(NbusHost));

	if ((self->socket = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		syslog(LOG_ERR, "cannot create CAN socket");
		return NBUS_RET_FAILED;
	}

	struct ifreq ifr = {.ifr_name = "can0"};
	ioctl(self->socket, SIOCGIFINDEX, &ifr);

	struct sockaddr_can addr = {
		.can_family = AF_CAN,
		.can_ifindex = ifr.ifr_ifindex
	};

	if (bind(self->socket, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		syslog(LOG_ERR, "cannot bind CAN socket");
		return NBUS_RET_FAILED;
	}

	struct timeval to = {.tv_sec = 1, .tv_usec = 0};
	if (setsockopt(self->socket, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof(to)) < 0) {
		syslog(LOG_ERR, "setsockopt failed (timeout)");
		return NBUS_RET_FAILED;
	}

	syslog(LOG_INFO, "CAN device %s opened, if index %d, bitrate %u", device, ifr.ifr_ifindex, bitrate);
	return NBUS_RET_OK;
}


nbus_ret_t nbus_host_free(NbusHost *self) {
	if (close(self->socket) < 0) {
		syslog(LOG_ERR, "cannot close CAN socket");
		return NBUS_RET_FAILED;
	}
	syslog(LOG_INFO, "CAN device closed");

	return NBUS_RET_OK;
}


nbus_ret_t nbus_host_start(NbusHost *self) {
	if (pthread_create(&self->can_rx_thread, NULL, nbus_host_can_rx_thread, self) != 0) {
		return NBUS_RET_FAILED;
	}
	if (pthread_create(&self->descriptor_thread, NULL, nbus_host_descriptor_thread, self) != 0) {
		return NBUS_RET_FAILED;
	}
	return NBUS_RET_OK;
}


nbus_ret_t nbus_host_join(NbusHost *self) {
	pthread_join(self->can_rx_thread, NULL);
	pthread_join(self->descriptor_thread, NULL);
	return NBUS_RET_OK;
}
