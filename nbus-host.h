/* SPDX-License-Identifier: BSD-2-Clause
 *
 * nbus
 *
 * Copyright (c) 2018-2023, Marek Koza (qyx@krtko.org)
 * All rights reserved.
 */

#pragma once

#include <stdint.h>
#include <pthread.h>


#include "nbus-channel.h"
#include "nbus-packet.h"




/**********************************************************************************************************************
 * NBUS channel & related declarations
 **********************************************************************************************************************/


typedef struct nbus_host {
	int socket;
	pthread_t can_rx_thread;
	pthread_t descriptor_thread;

	NbusChannel *channels;

} NbusHost;


nbus_ret_t nbus_host_init(NbusHost *self, const char *device, uint32_t bitrate);
nbus_ret_t nbus_host_free(NbusHost *self);
nbus_ret_t nbus_host_start(NbusHost *self);
nbus_ret_t nbus_host_join(NbusHost *self);
nbus_ret_t nbus_host_invalidate_channel(NbusHost *self, NbusChannel *c);
NbusChannel *nbus_host_find_channel(NbusHost *self, nbus_channel_short_id_t short_id);
NbusChannel *nbus_host_find_channel_act(NbusHost *self, nbus_channel_id_t channel_id);
