/* SPDX-License-Identifier: BSD-2-Clause
 *
 * nbus packet parsing and building
 * OpenWRT nwDaq companion project
 *
 * Copyright (c) 2023, Marek Koza (qyx@krtko.org)
 * All rights reserved.
 */


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#include "nbus-common.h"
#include "nbus-packet.h"


void nbus_parse_id(uint32_t id, struct nbus_id *sid) {
	sid->channel = (id >> 12) & 0xffff;
	sid->opcode = id & 0xff;
	sid->direction = (id >> 10) & 0x3;
}


uint32_t nbus_build_id(struct nbus_id *id) {
	uint32_t r = 0U;
	r += id->channel << 12;
	r += id->direction << 10;
	r += id->opcode;
	return r;
}


/**
 * @brief Initialise a transmit packet instance
 *
 * Beware the data buffer is not yet assigned nor allocated. Use @p nbus_txpacket_buf_ref or @p nbus_txpacket_buf_copy
 * to set the packet data.
 */
nbus_ret_t nbus_txpacket_init(NbusTxPacket *self, nbus_channel_id_t channel_id, nbus_endpoint_t ep, uint32_t packet_counter) {
	memset(self, 0, sizeof(NbusTxPacket));

	self->channel_id = channel_id;
	self->ep = ep;
	self->packet_counter = packet_counter;
	self->state = NBUS_TXP_LEADING;
	return NBUS_RET_OK;
}


nbus_ret_t nbus_txpacket_buf_ref(NbusTxPacket *self, void *buf, size_t len) {
	self->buf = buf;
	self->len = len;
	return NBUS_RET_OK;
}


nbus_ret_t nbus_txpacket_buf_copy(NbusTxPacket *self, void *buf, size_t len) {
	(void)self;
	(void)buf;
	(void)len;
	/** todo malloc() and copy */
	return NBUS_RET_FAILED;
}

nbus_ret_t nbus_txpacket_get_fragment(NbusTxPacket *self, struct nbus_id *id, void *data, size_t *len) {
	switch (self->state) {
		case NBUS_TXP_LEADING: {
			id->channel = self->channel_id;
			id->direction = NBUS_DIR_REQUEST;
			id->opcode = NBUS_OP_LEADING_MIN + self->ep;

			struct nbus_leading_frame_msg lfbuf = {
				/* For a peripheral mode channel, a packet is first received from the initiator saving the
				 * request packet counter. The response simply copies the packet counter value.
				 * For a initiator mode, the packet_counter variable contains the next valid value to be sent
				 * (it is incremented beforehand). When waiting for a response, we are expecting the same
				 * value as we sent before. */
				.counter = self->packet_counter,
				.len = self->len,
				.flags = 0
			};
			memcpy(data, &lfbuf, sizeof(lfbuf));
			*len = sizeof(lfbuf);

			self->packet_sent = 0;
			self->next_frag = 0;
			self->state = NBUS_TXP_DATA;
			break;
		}
		case NBUS_TXP_DATA: {
			/* Construct and send the frame containing the chunk data. */
			id->channel = self->channel_id;
			id->direction = NBUS_DIR_REQUEST;
			id->opcode = NBUS_OP_DATA_MIN + self->next_frag;

			size_t frag_len = self->len - self->packet_sent;
			/** @todo limit properly to CAN MTU */
			if (frag_len > 8) {
				frag_len = 8;
			}
			memcpy(data, (uint8_t *)self->buf + self->packet_sent, frag_len);
			*len = frag_len;

			self->packet_sent += frag_len;
			self->next_frag++;
			if (self->packet_sent == self->len) {
				self->state = NBUS_TXP_TRAILING;
			}
			break;
		}
		case NBUS_TXP_TRAILING: {
			id->channel = self->channel_id;
			id->direction = NBUS_DIR_REQUEST;
			id->opcode = NBUS_OP_TRAILING;

			uint8_t siv[NBUS_SIV_LEN] = {0};
			memcpy(data, siv, sizeof(siv));
			*len = sizeof(siv);

			self->state = NBUS_TXP_DONE;
			break;
		}
		case NBUS_TXP_DONE:
			return NBUS_RET_VOID;

		case NBUS_TXP_EMPTY:
		default:
			return NBUS_RET_FAILED;
	}

	return NBUS_RET_OK;
}


nbus_ret_t nbus_rxpacket_init(NbusRxPacket *self, size_t max_size) {
	memset(self, 0, sizeof(NbusRxPacket));

	self->buf = malloc(max_size);
	if (self->buf == NULL) {
		return NBUS_RET_FAILED;
	}
	self->buf_size = max_size;

	sem_init(&self->done, 0, 0);

	self->state = NBUS_RXP_READY;
	return NBUS_RET_OK;
}


nbus_ret_t nbus_rxpacket_free(NbusRxPacket *self) {
	free(self->buf);
	self->buf = NULL;
	self->buf_size = 0;
	sem_destroy(&self->done);
	return NBUS_RET_OK;
}


nbus_ret_t nbus_rxpacket_reset(NbusRxPacket *self) {
	self->state = NBUS_RXP_READY;
	return NBUS_RET_OK;
}


nbus_ret_t nbus_rxpacket_wait(NbusRxPacket *self, uint32_t timeout_ms) {
	/* Compute the timeout time */
	struct timespec ts = {0};
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_nsec += timeout_ms * 1000000;

	/* Normalise the timespec. */
	ts.tv_sec += ts.tv_nsec / 1000000000;
	ts.tv_nsec %= 1000000000;

	if (self->state == NBUS_RXP_DONE) {
		return NBUS_RET_OK;
	}
	if (sem_timedwait(&self->done, &ts) == 0) {
		return NBUS_RET_OK;
	}

	return NBUS_RET_VOID;
}


nbus_ret_t nbus_rxpacket_append(NbusRxPacket *self, struct nbus_id *id, void *buf, size_t len) {
	// u_log(system_log, LOG_TYPE_DEBUG, U_LOG_MODULE_PREFIX("rxpacket state %d, channel %u, op %u"), self->state, id->channel, id->opcode);

	if (self->state == NBUS_RXP_EMPTY) {
		return NBUS_RET_FAILED;
	}

	switch (id->opcode) {
		case NBUS_OP_LEADING_MIN ... NBUS_OP_LEADING_MAX: {
			/* Leading frame is always dominant wrt. packet processing. Whatever the actual state is,
			 * a leading frame always causes its reset. Consider generating a warning if a different
			 * endpoint packet is currently being assembled as this means the endpoint data is
			 * interleaved (forbidden condition). */
			nbus_endpoint_t ep = id->opcode - NBUS_OP_LEADING_MIN;
			struct nbus_leading_frame_msg *lf = buf;

			/* The previous packet was correctly finished and set to the DONE state but nobody
			 * bothered to get its data and reset the FSM back. */
			if (self->state == NBUS_RXP_DONE) {
				syslog(LOG_WARNING, "nobody receiving", self->state);
				self->state = NBUS_RXP_READY;
			}

			if (self->state != NBUS_RXP_READY) {
				/* Nope. Go back to ready, restart packet processing. */
				syslog(LOG_WARNING, "packet reassembly restarted state=%d", self->state);
				self->state = NBUS_RXP_READY;
			}

			/* Do not accept packets longer than the allowed MTU. Do not even bother receiving
			 * the rest of the packet (do not init). */
			if (lf->len > self->buf_size) {
				// u_log(system_log, LOG_TYPE_DEBUG, U_LOG_MODULE_PREFIX("packet len > MTU (%u)"), lf->len);
				return NBUS_RET_FAILED;
			}

			/* Loading frame accepted now. */
			self->packet_size = 0;
			self->expected_packet_size = lf->len;
			self->frame_expected = 0;
			self->ep = ep;

			self->state = NBUS_RXP_DATA;

			// u_log(system_log, LOG_TYPE_DEBUG, U_LOG_MODULE_PREFIX("new packet channel %u, ep %u, size %u"), self->channel_id, self->ep, self->packet_size);
			break;
		}

		case NBUS_OP_DATA_MIN ... NBUS_OP_DATA_MAX: {
			if (self->state != NBUS_RXP_DATA) {
				return NBUS_RET_FAILED;
			}
			uint32_t frame = id->opcode - NBUS_OP_DATA_MIN;
			if (self->frame_expected != frame) {
				// u_log(system_log, LOG_TYPE_DEBUG, U_LOG_MODULE_PREFIX("enexpected frame %u"), frame);
				return NBUS_RET_FAILED;
			}
			/* Do not receive more data than is the maximum packet size (checked during the leading
			 * frame processing). */
			if ((self->packet_size + len) > self->expected_packet_size) {
				// u_log(system_log, LOG_TYPE_DEBUG, U_LOG_MODULE_PREFIX("packet becoming too big %lu > %lu"), self->packet_size + len, self->expected_packet_size);
				return NBUS_RET_FAILED;
			}

			memcpy(self->buf + self->packet_size, buf, len);
			self->packet_size += len;
			self->frame_expected++;

			if (self->packet_size == self->expected_packet_size) {
				self->state = NBUS_RXP_TRAILING;
			}
			break;
		}

		case NBUS_OP_TRAILING: {
			if (self->state != NBUS_RXP_TRAILING) {
				/* Wrong state, log it. Do not process the data and invalidate FSM. */
				// u_log(system_log, LOG_TYPE_DEBUG, U_LOG_MODULE_PREFIX("invalid trailing frame opcode, packet incomplete"));
				self->state = NBUS_RXP_INVALID;
				return NBUS_RET_FAILED;
			}
			/* Do not check packet size. It is correct if the state is ok. */

			/** @todo check the MAC here */

			self->state = NBUS_RXP_DONE;
			sem_post(&self->done);
			break;
		}

		case NBUS_OP_ADVERTISEMENT:
			/* An advertisement opcode has been received for the same channel-id. This basically means
			 * there is a channel-id conflict on the bus. Retreat by invalidating the current channel-id. */
			return NBUS_RET_INVALID;

		default:
			return NBUS_RET_FAILED;
	}
	return NBUS_RET_OK;
}
