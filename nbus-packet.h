/* SPDX-License-Identifier: BSD-2-Clause
 *
 * nbus packet parsing and building
 * OpenWRT nwDaq companion project
 *
 * Copyright (c) 2023, Marek Koza (qyx@krtko.org)
 * All rights reserved.
 */

#pragma once

#include <stdint.h>
#include <semaphore.h>
#include <pthread.h>

#include "nbus-common.h"


struct __attribute__((__packed__)) nbus_leading_frame_msg {
	uint32_t counter;
	uint16_t len;
	uint16_t flags;
};

/**********************************************************************************************************************
 * TX packet declarations
 **********************************************************************************************************************/

enum nbus_txp_state {
	NBUS_TXP_EMPTY = 0,
	NBUS_TXP_LEADING,
	NBUS_TXP_DATA,
	NBUS_TXP_TRAILING,
	NBUS_TXP_DONE,
};

typedef struct nbus_txpacket {
	volatile enum nbus_txp_state state;

	nbus_channel_id_t channel_id;
	nbus_endpoint_t ep;
	uint32_t packet_counter;

	/* Progress variables */
	size_t next_frag;
	size_t packet_sent;

	/* Preallocated scratchpad. */
	void *buf;
	size_t len;
} NbusTxPacket;

/**********************************************************************************************************************
 * RX packet declarations
 **********************************************************************************************************************/

enum nbus_rxp_state {
	NBUS_RXP_EMPTY = 0,
	NBUS_RXP_READY,
	NBUS_RXP_DATA,
	NBUS_RXP_TRAILING,
	NBUS_RXP_DONE,
	NBUS_RXP_INVALID,
	NBUS_RXP_VOID,
	NBUS_RXP_INVALID_ID,
};

/* Buffer for assembling fragmented received packets. Beware there
 * is only one buffer for all endpoints. Endpoint packets cannot interleave. */
typedef struct nbus_rxpacket {
	volatile enum nbus_rxp_state state;

	uint8_t *buf;
	size_t buf_size;

	uint32_t frame_expected;
	size_t packet_size;
	size_t expected_packet_size;
	nbus_endpoint_t ep;

	sem_t done;

	/* Channel's mutex */
	pthread_mutex_t *mutex;
} NbusRxPacket;


void nbus_parse_id(uint32_t id, struct nbus_id *sid);
uint32_t nbus_build_id(struct nbus_id *id);

nbus_ret_t nbus_txpacket_init(NbusTxPacket *self, nbus_channel_id_t channel_id, nbus_endpoint_t ep, uint32_t packet_counter);
nbus_ret_t nbus_txpacket_buf_ref(NbusTxPacket *self, void *buf, size_t len);
nbus_ret_t nbus_txpacket_buf_copy(NbusTxPacket *self, void *buf, size_t len);
nbus_ret_t nbus_txpacket_get_fragment(NbusTxPacket *self, struct nbus_id *id, void *data, size_t *len);

nbus_ret_t nbus_rxpacket_init(NbusRxPacket *self, size_t max_size);
nbus_ret_t nbus_rxpacket_free(NbusRxPacket *self);
nbus_ret_t nbus_rxpacket_reset(NbusRxPacket *self);
nbus_ret_t nbus_rxpacket_wait(NbusRxPacket *self, uint32_t timeout_ms);
nbus_ret_t nbus_rxpacket_append(NbusRxPacket *self, struct nbus_id *id, void *buf, size_t len);

