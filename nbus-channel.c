/* SPDX-License-Identifier: BSD-2-Clause
 *
 * nbus channel handling
 * OpenWRT nwDaq companion project
 *
 * Copyright (c) 2023, Marek Koza (qyx@krtko.org)
 * All rights reserved.
 */


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include "tinycbor/src/cbor.h"

#include "nbus-common.h"
#include "nbus-host.h"
#include "nbus-channel.h"


static nbus_ret_t nbus_channel_send_frame(NbusChannel *self, struct nbus_id *id, void *buf, size_t len) {
	NbusHost *nbus = self->nbus;
	if (len > 8) {
		return NBUS_RET_FAILED;
	}

	struct can_frame frame = {0};
	/* Mark the ID as extended */
	frame.can_id = nbus_build_id(id) | (1 << 31);
	frame.can_dlc = len;
	memcpy(frame.data, buf, len);

	// printf("---> frame channel=0x%04x, opcode=0x%02x, direction=%d, len=%d\n", id->channel, id->opcode, id->direction, len);
	// printf("     id=%08x dlc=%d\n", frame.can_id, frame.can_dlc);

	int wbytes = write(self->nbus->socket, &frame, sizeof(struct can_frame));
	if (wbytes != sizeof(struct can_frame)) {
		return NBUS_RET_FAILED;
	}

	return NBUS_RET_OK;
}



nbus_ret_t nbus_channel_send(NbusChannel *self, nbus_endpoint_t ep, void *buf, size_t len) {
	if (len > NBUS_CHANNEL_MTU) {
		return NBUS_RET_BAD_PARAM;
	}

	/* Acquire the channel lock to prevent multiple threads sending interleaved data on multiple
	 * endpoints and sending any data when a reception is ongoing. */
	/** @todo mutex cannot be locked when a fragmentation error occured in the past, see unlock at the end too. */
	/* pthread_mutex_lock(&self->mutex); */
	nbus_txpacket_init(&self->txpacket, self->channel_id, ep, self->packet_counter);
	nbus_txpacket_buf_ref(&self->txpacket, buf, len);

	struct nbus_id sid = {0};
	uint8_t framebuf[8];
	size_t framelen = 0;
	while (nbus_txpacket_get_fragment(&self->txpacket, &sid, framebuf, &framelen) == NBUS_RET_OK) {
		nbus_channel_send_frame(self, &sid, framebuf, framelen);
	}
	self->packet_counter++;
	/* pthread_mutex_unlock(&self->mutex); */

	return NBUS_RET_OK;
}


nbus_ret_t nbus_channel_receive(NbusChannel *self, nbus_endpoint_t *ep, void *buf, size_t buf_size, size_t *len, uint32_t timeout_ms) {
	nbus_ret_t ret = nbus_rxpacket_wait(&self->rxpacket, timeout_ms);
	/* Release the channel mutext now in any circumstances. If a timeout occured, we are not interested
	 * in correct reception anymore. */
	/** @todo mutex is not unlocked when a fragmentation error occurs */
	/* pthread_mutex_unlock(&self->mutex); */
	if (ret != NBUS_RET_OK) {
		return ret;
	}

	if (self->rxpacket.packet_size > buf_size) {
		nbus_rxpacket_reset(&self->rxpacket);
		return NBUS_RET_BIG;
	}
	memcpy(buf, self->rxpacket.buf, self->rxpacket.packet_size);
	*len = self->rxpacket.packet_size;
	*ep = self->rxpacket.ep;

	/* Prepare for the next packet. */
	nbus_rxpacket_reset(&self->rxpacket);
	return NBUS_RET_OK;
}


nbus_ret_t nbus_channel_exchange(NbusChannel *self, nbus_endpoint_t ep, void *txbuf, size_t txlen, void *rxbuf, size_t rxsize, size_t *rxlen, uint32_t timeout_ms) {
	pthread_mutex_lock(&self->ex_mutex);

	if (nbus_channel_send(self, ep, txbuf, txlen) != NBUS_RET_OK) {
		pthread_mutex_unlock(&self->ex_mutex);
		return NBUS_RET_FAILED;
	}
	nbus_endpoint_t rxep = 0;
	nbus_ret_t ret = nbus_channel_receive(self, &rxep, rxbuf, rxsize, rxlen, timeout_ms);
	if (ret == NBUS_RET_OK && ep != rxep) {
		syslog(LOG_ERR, "[%08x] received non matching endpoint %d != %d", self->short_id, ep, rxep);
	}

	pthread_mutex_unlock(&self->ex_mutex);
	return ret;
}


nbus_ret_t nbus_channel_get_descriptors(NbusChannel *self) {
	/* Do not get descriptors again. @todo DO get them again */
	if (self->desc_ok) {
		return NBUS_RET_VOID;
	}

	/* Encode request for name, interface and version into a CBOR object. */
	uint8_t txbuf[128] = {0};
	CborEncoder encoder, encoder_map;
	cbor_encoder_init(&encoder, txbuf, sizeof(txbuf), 0);
	/* Create the top level map. */
	cbor_encoder_create_map(&encoder, &encoder_map, CborIndefiniteLength);
	cbor_encode_text_stringz(&encoder_map, "n");
	cbor_encode_null(&encoder_map);
	cbor_encode_text_stringz(&encoder_map, "if");
	cbor_encode_null(&encoder_map);
	cbor_encode_text_stringz(&encoder_map, "v");
	cbor_encode_null(&encoder_map);
	cbor_encoder_close_container(&encoder, &encoder_map);
	size_t txlen = cbor_encoder_get_buffer_size(&encoder, txbuf);

	/* Send the request and get the descriptor back. */
	uint8_t rxbuf[256] = {0};
	size_t rxlen = 0;
	if (nbus_channel_exchange(self, 0, txbuf, txlen, rxbuf, sizeof(rxbuf), &rxlen, 50) != NBUS_RET_OK) {
		return NBUS_RET_FAILED;
	}

	CborParser parser;
	CborValue value;
	cbor_parser_init(rxbuf, rxlen, 0, &parser, &value);
	if (!cbor_value_is_map(&value)) {
		return NBUS_RET_FAILED;
	}
	CborValue value_map;
	cbor_value_enter_container(&value, &value_map);
	while (!cbor_value_at_end(&value_map)) {
		if (!cbor_value_is_text_string(&value_map)) {
			/* Only string keys allowed */
			return NBUS_RET_FAILED;
		}
		char key[32] = {0};
		size_t key_len = sizeof(key);
		cbor_value_copy_text_string(&value_map, key, &key_len, NULL);
		if (key_len == sizeof(key)) {
			/* String did not fit inside the provided buffer. */
			return NBUS_RET_FAILED;
		}
		cbor_value_advance(&value_map);

		if (!cbor_value_is_text_string(&value_map)) {
			/* Only string values allowed */
			return NBUS_RET_FAILED;
		}
		char value[128] = {0};
		size_t value_len = sizeof(value);
		cbor_value_copy_text_string(&value_map, value, &value_len, NULL);
		if (value_len == sizeof(value)) {
			/* String did not fit inside the provided buffer. */
			return NBUS_RET_FAILED;
		}
		cbor_value_advance(&value_map);

		if (!strcmp(key, "n")) {
			strlcpy(self->name, value, NBUS_DESC_NAME_MAX);
		}
		if (!strcmp(key, "if")) {
			strlcpy(self->interface, value, NBUS_DESC_INTERFACE_MAX);
		}
		if (!strcmp(key, "v")) {
			strlcpy(self->version, value, NBUS_DESC_VERSION_MAX);
		}
	}

	syslog(LOG_INFO, "[%08x] descriptors name=%s if=%s version=%s", self->short_id, self->name, self->interface, self->version);
	self->desc_ok = true;

	return NBUS_RET_OK;
}


nbus_ret_t nbus_channel_init(NbusChannel *self, NbusHost *parent) {
	memset(self, 0, sizeof(NbusChannel));

	self->nbus = parent;

	/* RX packet instance is created only once. */
	nbus_rxpacket_init(&self->rxpacket, NBUS_CHANNEL_MTU);

	pthread_mutex_init(&self->mutex, NULL);
	pthread_mutex_init(&self->ex_mutex, NULL);

	return NBUS_RET_OK;
}
