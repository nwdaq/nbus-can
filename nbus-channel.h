/* SPDX-License-Identifier: BSD-2-Clause
 *
 * nbus channel handling
 * OpenWRT nwDaq companion project
 *
 * Copyright (c) 2023, Marek Koza (qyx@krtko.org)
 * All rights reserved.
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

#include "nbus-common.h"
#include "nbus-packet.h"

typedef struct nbus_host NbusHost;

typedef struct nbus_channel {
	NbusHost *nbus;
	NbusChannel *next;
	NbusChannel *parent;

	/* Basic channel parameters. */
	nbus_channel_id_t channel_id;
	bool channel_id_valid;
	nbus_channel_short_id_t short_id;

	/* Descriptor content */
	bool desc_ok;
	char name[NBUS_DESC_NAME_MAX];
	char interface[NBUS_DESC_INTERFACE_MAX];
	char version[NBUS_DESC_VERSION_MAX];

	/* Packet instances. */
	NbusTxPacket txpacket;
	NbusRxPacket rxpacket;

	/* RPC client instance for EP 0 channel C&C. */
	// CborRpc ccrpc;
	// struct cbor_rpc_handler name_handler;
	// struct cbor_rpc_handler parent_handler;
	// struct cbor_rpc_handler interface_handler;
	// struct cbor_rpc_handler version_handler;

	/* A monotonically increasing counter. The calue is increased by one
	 * every time a discovery probe response message is sent. */
	uint32_t counter;

	uint32_t packet_counter;

	/* Blake2s-SIV message authentication and ancryption */
	// uint8_t key[NBUS_KEY_SIZE];
	// uint8_t ke[B2S_KE_LEN];
	// uint8_t km[B2S_KM_LEN];
	time_t adv_time;

	pthread_mutex_t mutex;
	pthread_mutex_t ex_mutex;

} NbusChannel;


nbus_ret_t nbus_channel_send(NbusChannel *self, nbus_endpoint_t ep, void *buf, size_t len);
nbus_ret_t nbus_channel_receive(NbusChannel *self, nbus_endpoint_t *ep, void *buf, size_t buf_size, size_t *len, uint32_t timeout_ms);
nbus_ret_t nbus_channel_exchange(NbusChannel *self, nbus_endpoint_t ep, void *txbuf, size_t txlen, void *rxbuf, size_t rxsize, size_t *rxlen, uint32_t timeout_ms);
nbus_ret_t nbus_channel_get_descriptors(NbusChannel *self);
nbus_ret_t nbus_channel_init(NbusChannel *self, NbusHost *parent);
