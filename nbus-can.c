#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <syslog.h>

#include "nbus-host.h"

/* Default config because lazy to do proper argument parsing */
#define CONFIG_HOST "0.0.0.0"
#define CONFIG_PORT "4002"
#define CONFIG_CAN_DEVICE "can0"
#define CONFIG_CAN_BITRATE 1000000


struct nbus_zmq_packet {
	uint32_t short_id;
	uint32_t timestamp;
	uint8_t endpoint;
	/* 3 bytes of padding */
	uint8_t r1;
	uint16_t r2;
	/* 4 bytes of padding */
	uint32_t r3;
};


int main (void) {
	/* Setup logging */
	openlog("nbus", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_DAEMON);

	/* Start nbus protocol handling. The reception continues in a separate thread. */
	NbusHost host;
	nbus_host_init(&host, CONFIG_CAN_DEVICE, CONFIG_CAN_BITRATE);
	nbus_host_start(&host);

	/* Process ZMQ API on the main thread. It is a fully blocking API. */
	void *context = zmq_ctx_new();
	void *responder = zmq_socket(context, ZMQ_REP);
	int rc = zmq_bind(responder, "tcp://0.0.0.0:4000");

	while (1) {
		unsigned char buf[sizeof(struct nbus_zmq_packet) + NBUS_CHANNEL_MTU] = {0};
		int len = zmq_recv(responder, buf, sizeof(buf), 0);
		if (len < sizeof(struct nbus_zmq_packet)) {
			syslog(LOG_WARNING, "malformed rx message");
			/* Not enough data received, even the heder does not fit. */
			goto zmq_supplement_send;
		}
		if (len == -1) {
			syslog(LOG_ERR, "error while reading from the ZMQ socket: %m");
			goto zmq_supplement_send;
		}

		/* Packet header is always at the beginning. */
		struct nbus_zmq_packet *hdr = (struct nbus_zmq_packet *)buf;

		/* 0xd52f3f42 */
		/* Try to find the corresponding channel and send the data if found. */
		NbusChannel *ch = nbus_host_find_channel(&host, hdr->short_id);
		if (ch != NULL) {
			size_t rxlen = 0;
			nbus_ret_t ret = nbus_channel_exchange(
				ch,
				hdr->endpoint,
				/* The data to send starts right after the header. The length is changed accordingly. */
				buf + sizeof(struct nbus_zmq_packet),
				len - sizeof(struct nbus_zmq_packet),
				/* Same for the received data, reserve space for the header */
				buf + sizeof(struct nbus_zmq_packet),
				sizeof(buf) - sizeof(struct nbus_zmq_packet),
				&rxlen,
				30
			);
			if (ret == NBUS_RET_OK) {
				/* hdr is still set to the beginning of the buffer, the content is kept. */
				zmq_send(responder, buf, rxlen + sizeof(struct nbus_zmq_packet), 0);
			} else {
				syslog(LOG_WARNING, "nbus communication error, ret = %d", ret);
				goto zmq_supplement_send;
			}
		} else {
			// syslog(LOG_WARNING, "cannot find the requested nbus channel %08x", hdr->short_id);
			goto zmq_supplement_send;
		}
		/* Explicit to skip the supplemental send */
		continue;

zmq_supplement_send:
		zmq_send(responder, buf, sizeof(struct nbus_zmq_packet), 0);

	}

	zmq_close(responder);
	nbus_host_join(&host);
	nbus_host_free(&host);
	closelog();
}
