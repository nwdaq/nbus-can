all:
	$(CC) ${CFLAGS} -c nbus-can.c
	$(CC) ${CFLAGS} -c nbus-packet.c
	$(CC) ${CFLAGS} -c nbus-host.c
	$(CC) ${CFLAGS} -c nbus-channel.c
	$(CC) ${CFLAGS} -c tinycbor/src/cborparser.c
	$(CC) ${CFLAGS} -c tinycbor/src/cborencoder.c

	$(CC) ${CFLAGS} ${LDFLAGS} nbus-can.o nbus-host.o nbus-channel.o nbus-packet.o cborparser.o cborencoder.o -o nbus-can -lzmq -lsodium -L/usr/lib/ -I /usr/include/


