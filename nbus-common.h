#pragma once

typedef enum {
	NBUS_RET_OK = 0,
	NBUS_RET_FAILED,
	NBUS_RET_BAD_PARAM,
	NBUS_RET_VOID,
	NBUS_RET_INVALID,
	NBUS_RET_BIG,
} nbus_ret_t;


typedef uint8_t nbus_opcode_t;
typedef uint8_t nbus_endpoint_t;

#define NBUS_OP_LEADING_MIN 0x00
#define NBUS_OP_LEADING_MAX 0x3f
#define NBUS_OP_DATA_MIN 0x40
#define NBUS_OP_DATA_MAX 0xbf
#define NBUS_OP_TRAILING 0xc0
#define NBUS_OP_ADVERTISEMENT 0xc1

#define NBUS_CHANNEL_MTU (64*8)

#define NBUS_KEY_SIZE 16
#define NBUS_SIV_LEN 8

#define NBUS_ADV_TIMEOUT_S 20

#define NBUS_DESC_NAME_MAX 128
#define NBUS_DESC_VERSION_MAX 32
#define NBUS_DESC_INTERFACE_MAX 32

enum nbus_direction {
	NBUS_DIR_REQUEST = 0,
	NBUS_DIR_RESPONSE = 1,
	NBUS_DIR_PUBLISH = 2,
	NBUS_DIR_SUBSCRIBE = 3
};

typedef uint16_t nbus_channel_id_t;
typedef uint32_t nbus_channel_short_id_t;
typedef struct nbus_channel NbusChannel;

struct __attribute__((__packed__)) nbus_id {
	uint8_t res1:4;
	nbus_channel_id_t channel:16;
	enum nbus_direction direction:2;
	uint8_t res2:2;
	nbus_opcode_t opcode:8;
};

